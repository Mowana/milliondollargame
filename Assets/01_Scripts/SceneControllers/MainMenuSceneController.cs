﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuSceneController : SceneController {


    #region UI Utilities
    public void GoToGame()
    {
        SceneModel heroMenuModel = new SceneModel();
        heroMenuModel.sceneIndex = SceneIndexEnum.HeroMenu;
        SceneTransitionManager.Instance.AddSceneAsync(heroMenuModel);
    }
    #endregion
}
