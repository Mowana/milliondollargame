﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneModel
{
    public SceneIndexEnum sceneIndex;
}

public class SceneController : MonoBehaviour {

	private SceneModel _sceneModel;
    public SceneModel SceneModel
    {
        get { return _sceneModel; }
    }

    protected bool _isInitialized = false;

    /// <summary>
    /// Main initializer for scene controllers.
    /// </summary>
    public virtual IEnumerator InitSequence(SceneModel sceneModel)
    {
        _sceneModel = sceneModel;
        _isInitialized = true;
        yield break;
    }

    /// <summary>
    /// Event raised by the SceneTransitionManager when the instance of a scene is finished loading.
    /// </summary>
    /// <returns>True in case an additive scene was triggered to be loaded on post load. Otherwise, false.</returns>
    public virtual IEnumerator OnPostLoading()
    {
        yield break;
    }

    /// <summary>
    /// Called before the scene is destroyed
    /// </summary>
    /// <returns></returns>
    public virtual IEnumerator OnDispose()
    {
        yield break;
    }

    public virtual void OnFocus()
    {}
}
