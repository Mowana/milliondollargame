﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Linq;

/// <summary>
/// Main enumeration for scenes indexation. Please respect the scene name shown in Build Settings.
/// </summary>
public enum SceneIndexEnum
{
    MainMenu,
    HeroMenu,
    Max
}

public class SceneTransitionManager : Singleton<SceneTransitionManager> {

    #region Events  
    public delegate void CurrentSceneChange(SceneController currentScene);
    public event CurrentSceneChange OnCurrentSceneChange;
    #endregion

    private bool _isLoading = false;
    private bool _isClosingAdditiveScene = false;

    public bool IsAvailableForTransition()
    {
        return !_isLoading && !_isClosingAdditiveScene;
    }

    private SceneController _currentScene;
    public SceneController CurrentSceneController
    {
        get { return _currentScene; }
        private set
        {
            _currentScene = value;
        }
    }

    private SceneModel _currentSceneModel;
    public SceneModel CurrentSceneModel
    {
        get { return _currentSceneModel; }
        private set
        {
            _currentSceneModel = value;
            if (OnCurrentSceneChange != null)
            {
                OnCurrentSceneChange(_currentScene);
            }
        }
    }
    private SceneModel _previousSceneModel;

    private SceneIndexEnum _sceneIndexRequestedForLoading = SceneIndexEnum.Max;

    // additive scene stack to enable back button logic
    private IndexedStack<SceneController> _additiveSceneStack = new IndexedStack<SceneController>();

    protected override void OnAwake()
    {
        // get the current scene who called the singleton
        StartCoroutine(LateInitialization());

    }

    private IEnumerator LateInitialization()
    {
        SceneController controller = null;

        while (true)
        {
            var activeScene = SceneManager.GetActiveScene();

            if (activeScene.isLoaded)
            {
                controller = GetSceneController(activeScene.GetRootGameObjects());

                if (controller != null) break;
            }

            yield return null;
        }

        CurrentSceneController = controller;
        _previousSceneModel = null;
        yield return StartCoroutine(WaitForGameInitialization());
    }

    private IEnumerator WaitForGameInitialization()
    {
        /* [JC] todo: reenable this when there is a global initialization logic
        while (InitializationManager.Instance == null || !InitializationManager.Instance.IsInitializationReady)
        {
            yield return null;
        }

        if (_currentScene == null)
        {
            Debug.LogWarning("Unable to wait for game initialization for an invalid scene. Hiding loading screen and stoping coroutine...");
            if (InitializationManager.Instance != null && InitializationManager.Instance.LoadingScreenLogic != null)
            {
                InitializationManager.Instance.LoadingScreenLogic.Hide();
            }
            yield break;
        }
        */

        Debug.Log("Game Initialization -> Scene name: " + _currentScene.name);
        yield return StartCoroutine(_currentScene.InitSequence(null));
        CurrentSceneModel = _currentScene.SceneModel;
        yield return StartCoroutine(_currentScene.OnPostLoading());

        /* [JC] todo: reenable this when there is a global initialization logic
        if (InitializationManager.Instance != null && InitializationManager.Instance.LoadingScreenLogic != null)
        {
            InitializationManager.Instance.LoadingScreenLogic.Hide();
        }*/
    }

    #region Scene Transition
    public IEnumerator AddSceneAsync(SceneModel sceneToAdd, bool closeLastAdditiveScene = false, bool closeAllAditiveScenes = false, bool queueLoad = false)
    {
        yield return StartCoroutine(AddSceneAsync(sceneToAdd.sceneIndex, sceneToAdd, closeLastAdditiveScene, closeAllAditiveScenes, queueLoad));
    }

    public IEnumerator AddSceneAsync(SceneIndexEnum sceneToAdd, SceneModel model, bool closeLastAdditiveScene = false, bool closeAllAdditiveScene = false, bool queueLoad = false)
    {
        if (_isLoading)
        {
            if (!queueLoad)
            {
                Debug.Log("Already loading a scene! Scene: " + _currentSceneModel.sceneIndex.ToString());
                yield break;
            }

            // If using queue, the scenes will be added in the order the couroutines were called
            while (_isLoading)
            {
                yield return new WaitForEndOfFrame();
            }
        }
        _isLoading = true;
        AsyncOperation loadSceneOperation = SceneManager.LoadSceneAsync(sceneToAdd.ToString(), LoadSceneMode.Additive);
        yield return StartCoroutine(WaitingForAdditiveOperation(loadSceneOperation, sceneToAdd, model, closeLastAdditiveScene, closeAllAdditiveScene));
    }

    /// <summary>
    /// Main method to call a scene transition. You can create an ad-hok model for a particular scene. Don't pass a empty new SceneModel as argument!
    /// </summary>
    /// <param name="sceneToLoad"></param>
    /// <param name="model"></param>
    /// <returns></returns>
    public IEnumerator LoadSceneAsync(SceneIndexEnum sceneToLoad, SceneModel model = null)
    {
        if (_isLoading)
        {
            Debug.Log("Already loading a scene! Scene: " + _currentSceneModel.sceneIndex.ToString());
            yield break;
        }

        _isLoading = true;
        _sceneIndexRequestedForLoading = sceneToLoad;
        AsyncOperation loadSceneOperation = SceneManager.LoadSceneAsync(sceneToLoad.ToString(), LoadSceneMode.Single);
        yield return StartCoroutine(WaitingForLoadingOperation(loadSceneOperation, sceneToLoad, model));
    }

    private IEnumerator WaitingForAdditiveOperation(AsyncOperation loadSceneOperation, SceneIndexEnum sceneToAdd, SceneModel model,
                                                    bool closeLastAdditiveScene, bool closeAllAdditiveScene)
    {
        // Blocking InputManager's gesture logic while there's a additive scene in screen
        /* [JC]todo: Recover this section if there is need for input management
        bool blockedInput = false;
        if (InputManager.Instance.IsInputActive())
        {
            blockedInput = true;
            InputManager.Instance.DeactivateInput();
        }
        */

        while (!loadSceneOperation.isDone)
        {
            yield return null;
        }

        Scene additiveScene = SceneManager.GetSceneByName(sceneToAdd.ToString());
        if (!string.IsNullOrEmpty(additiveScene.name))
        {
            GameObject[] rootGameObjects = additiveScene.GetRootGameObjects();
            SceneController asc = GetSceneController(rootGameObjects);

            if (closeAllAdditiveScene)
            {
                // checking if there's any additive scene in current scene, remove them all. Don't clean resoruces for now.
                while (IsAnyAdditiveSceneActive())
                {
                    yield return StartCoroutine(CloseCurrentAdditiveSceneRoutine(false));
                }
            }
            else
            {
                if (closeLastAdditiveScene && _additiveSceneStack.Count > 0)
                {
                    SceneController sc = _additiveSceneStack.Pop();
                    yield return StartCoroutine(RemoveSceneFromMemory(sc));
                }
            }

            _additiveSceneStack.Push(asc);
            SceneManager.MergeScenes(additiveScene, SceneManager.GetActiveScene());

            yield return StartCoroutine(asc.InitSequence(model));

            if (OnCurrentSceneChange != null)
            {
                OnCurrentSceneChange(asc);
            }
        }

        /* [JC] todo: Recover this section if there is need for input management
        if (blockedInput)
        {
            InputManager.Instance.ActivateInput();
        }
        */
        _isLoading = false;

        //Check if the additive scene that was just loaded has any process to do now that it's loaded
        if (IsAnyAdditiveSceneActive())
        {
            SceneController sceneToCheck = _additiveSceneStack.Peek();
            yield return StartCoroutine(sceneToCheck.OnPostLoading());
            // [JC] todo: Re enable this if there is a loading screen logic
            //InitializationManager.Instance.LoadingScreenLogic.Hide();
        }

        yield break;
    }

    /// <summary>
    /// Core utility to close all additive scenes
    /// </summary>
    public void CloseAllAditiveScenes()
    {
        StartCoroutine(RequestCloseAllAditiveScenes());
    }

    /// <summary>
    /// Core utility to close all additive scenes (no focus)
    /// </summary>
    public void CloseAllAditiveScenesNoFocus()
    {
        StartCoroutine(RequestCloseAllAditiveScenesNoFocus());
    }

    private IEnumerator RequestCloseAllAditiveScenes()
    {
        // checking if there's any additive scene in current scene, remove them all. Don't clean resoruces for now.
        while (IsAnyAdditiveSceneActive())
        {
            yield return StartCoroutine(CloseCurrentAdditiveSceneRoutine(false));
        }
    }

    private IEnumerator RequestCloseAllAditiveScenesNoFocus()
    {
        while (IsAnyAdditiveSceneActive())
        {
            var toRemove = _additiveSceneStack.Peek();
            yield return StartCoroutine(CloseAdditiveSceneRoutine(toRemove, false, false));
        }
    }

    private IEnumerator WaitingForLoadingOperation(AsyncOperation loadSceneOperation, SceneIndexEnum sceneIndex, SceneModel model = null)
    {
        // [JC]todo: Re enable this if there is a loading screen logic
        // InitializationManager.Instance.LoadingScreenLogic.ActivateLoadingLoop();
        // InputManager.Instance.DeactivateInput();

        // Dispose previous scene
        if (_currentScene != null)
        {
            // call OnDispose for current scene. There is no need for unloading a main scene
            yield return StartCoroutine(_currentScene.OnDispose());
        }

        CurrentSceneController = null;

        // [JC]todo: Re enable this if there is a loading screen logic
        // InputManager.Instance.ResetEvents();

        // checking if there's any additive scene in current scene, remove them all. Don't clean resoruces for now.
        while (IsAnyAdditiveSceneActive())
        {
            yield return StartCoroutine(CloseCurrentAdditiveSceneRoutine(false));
        }

        while (!loadSceneOperation.isDone)
        {
            yield return null;
        }

        _previousSceneModel = _currentSceneModel;

        Scene loadedScene = SceneManager.GetSceneByName(sceneIndex.ToString());
        if (!string.IsNullOrEmpty(loadedScene.name))
        {
            GameObject[] rootGameObjects = loadedScene.GetRootGameObjects();

            SceneController controller = GetSceneController(rootGameObjects);
            if (controller == null)
            {
                Debug.Log("Current Scene Controller is null! Check the main scene object!");
                // [JC]todo: Re enable this if there is a loading screen logic
                // InputManager.Instance.ActivateInput();
                _sceneIndexRequestedForLoading = SceneIndexEnum.Max;
                yield break;
            }
            CurrentSceneController = controller;

            Debug.Log("Scene Transition clean process");
            System.GC.Collect();
            AsyncOperation unloadOperation = Resources.UnloadUnusedAssets();
            while (!unloadOperation.isDone)
            {
                yield return null;
            }

            Debug.Log("Scene Transition new controller's initSequence");
            yield return StartCoroutine(controller.InitSequence(model));
            CurrentSceneModel = controller.SceneModel;
        }
        else
        {
            Debug.LogError("Cannot find the current loaded scene!!! Name: " + sceneIndex.ToString());
        }

        yield return new WaitForEndOfFrame();

        _isLoading = false;
        _sceneIndexRequestedForLoading = SceneIndexEnum.Max;
        
        // [JC]todo: Re enable this if there is a loading screen logic
        // InputManager.Instance.ActivateInput();

        //Raise event of the loading process being ready
        yield return StartCoroutine(_currentScene.OnPostLoading());

        // [JC]todo: Re enable this if there is a loading screen logic
        //InitializationManager.Instance.LoadingScreenLogic.Hide();

        yield break;
    }

    private SceneController GetPreviousSceneController()
    {
        if (_additiveSceneStack.Count > 1)
        {   // previous is current minus one (count -2)
            return _additiveSceneStack[_additiveSceneStack.Count - 2];
        }
        return _currentScene;
    }

    /// <summary>
    /// Use this if you want to go back to the last scene.
    /// </summary>
    /// <returns></returns>
    public IEnumerator BackToLastScene()
    {
        if (_previousSceneModel == null)
        {
            Debug.LogWarning("There's no previous scene!!");
            yield break;
        }
        yield return StartCoroutine(LoadSceneAsync(_previousSceneModel.sceneIndex, _previousSceneModel));
        yield break;
    }

    public bool IsAnyAdditiveSceneActive()
    {
        return _additiveSceneStack.Count > 0;
    }

    public void CloseCurrentAdditiveScene()
    {
        StartCoroutine(CloseCurrentAdditiveSceneRoutine());
    }

    public void CloseAdditiveScene(SceneController sceneToClose)
    {
        StartCoroutine(CloseAdditiveSceneRoutine(sceneToClose));
    }

    public void RequestFocusOnLatestScene()
    {
        if (_additiveSceneStack.Count > 0)
        {
            if (_additiveSceneStack.Count == 0)
            {
                if (_currentScene != null)
                {
                    _currentScene.OnFocus();
                }
            }
            else
            {
                _additiveSceneStack.Peek().OnFocus();
            }
        }
    }

    private IEnumerator CloseCurrentAdditiveSceneRoutine(bool cleaningResources = true)
    {
        if (_isClosingAdditiveScene)
        {
            Debug.LogWarning("Already closing another additive scene!");
            yield break;
        }

        _isClosingAdditiveScene = true;
        if (_isLoading)
        {
            yield return null;
        }

        if (_additiveSceneStack.Count > 0)
        {
            SceneController sc = _additiveSceneStack.Pop();

            yield return StartCoroutine(RemoveSceneFromMemory(sc));

            if (_additiveSceneStack.Count == 0)
            {
                if (_currentScene != null)
                {
                    _currentScene.OnFocus();
                    if (OnCurrentSceneChange != null)
                    {
                        OnCurrentSceneChange(_currentScene);
                    }
                }
            }
            else
            {
                _additiveSceneStack.Peek().OnFocus();
                if (OnCurrentSceneChange != null)
                {
                    OnCurrentSceneChange(_additiveSceneStack.Peek());
                }
            }
        }

        if (cleaningResources)
        {
            System.GC.Collect();
            AsyncOperation unloadOperation = Resources.UnloadUnusedAssets();
            while (!unloadOperation.isDone)
            {
                yield return null;
            }
        }

        _isClosingAdditiveScene = false;
        yield break;
    }

    private IEnumerator CloseAdditiveSceneRoutine(SceneController sceneToClose, bool focusLastAdditiveScene = true, bool cleaningResources = true)
    {
        if (_isClosingAdditiveScene)
        {
            Debug.LogWarning("Already closing another additive scene!");
            yield break;
        }

        _isClosingAdditiveScene = true;
        if (_isLoading)
        {
            yield return null;
        }

        if (_additiveSceneStack.Count > 0)
        {
            SceneController sc = _additiveSceneStack.Find(w => w.Equals(sceneToClose));
            if (sc != null)
            {
                _additiveSceneStack.Remove(sc); // remove scene from stack
                yield return StartCoroutine(RemoveSceneFromMemory(sc)); // remove scene from memory

                if (_additiveSceneStack.Count == 0)
                {
                    if (_currentScene != null)
                    {
                        _currentScene.OnFocus();
                        if (OnCurrentSceneChange != null)
                        {
                            OnCurrentSceneChange(_currentScene);
                        }
                    }
                }
                else if (focusLastAdditiveScene)
                {
                    _additiveSceneStack.Peek().OnFocus();
                    if (OnCurrentSceneChange != null)
                    {
                        OnCurrentSceneChange(_additiveSceneStack.Peek());
                    }
                }
            }
        }

        if (cleaningResources)
        {
            System.GC.Collect();
            AsyncOperation unloadOperation = Resources.UnloadUnusedAssets();
            while (!unloadOperation.isDone)
            {
                yield return null;
            }
        }

        _isClosingAdditiveScene = false;
        yield break;
    }

    #endregion
    #region Utilities
    public IEnumerator ResetScene()
    {
        if (_currentSceneModel == null)
        {
            Debug.LogError("There's no current scene?!");
            yield break;
        }
        yield return StartCoroutine(LoadSceneAsync(_currentSceneModel.sceneIndex, _currentSceneModel));
        yield break;
    }

    public SceneIndexEnum GetLastSceneIndexOnScreen()
    {
        if (_additiveSceneStack.Count > 0)
        {
            SceneController sc = _additiveSceneStack.Peek();

            if (sc != null && sc.SceneModel != null)
            {
                return sc.SceneModel.sceneIndex;
            }
            else
            {
                return SceneIndexEnum.Max;
            }
        }

        // no additive screens in game
        if (_currentSceneModel != null)
            return _currentSceneModel.sceneIndex;
        else
            return SceneIndexEnum.Max;

    }

    /// <summary>
    /// Returns the scene index for the current non additive loaded scene
    /// </summary>
    /// <returns></returns>
    public SceneIndexEnum GetCurrentSceneIndex()
    {
        if (_currentSceneModel != null)
            return _currentSceneModel.sceneIndex;

        return SceneIndexEnum.Max;
    }

    public bool IsCurrentSceneIndex(SceneIndexEnum index)
    {
        return GetCurrentSceneIndex() == index;
    }

    public bool IsSceneIndexOnAdditiveStack(SceneIndexEnum index)
    {
        if (_additiveSceneStack != null && _additiveSceneStack.Count > 0)
        {
            return _additiveSceneStack.Any(x => x.SceneModel != null && x.SceneModel.sceneIndex == index);
        }

        return false;
    }

    public SceneController GetLastSceneControllerOnScreenOfType(SceneIndexEnum type)
    {
        if (_additiveSceneStack.Count > 0)
        {
            for (int i = 0, max = _additiveSceneStack.Count; i < max; i++)
            {
                SceneController targetScene = _additiveSceneStack[i];
                if (targetScene != null && targetScene.SceneModel != null && targetScene.SceneModel.sceneIndex == type)
                    return targetScene;
            }
        }

        if (_currentScene != null && _currentScene.SceneModel != null && _currentScene.SceneModel.sceneIndex == type)
        {
            return _currentScene;
        }

        return null; // target scene couldn't be found
    }

    public bool IsAnySceneRequestedForLoadingNow()
    {
        return !IsSceneRequestedForLoadingNow(SceneIndexEnum.Max);
    }
    public SceneIndexEnum GetCurrentIndexRequestedForLoading()
    {
        return _sceneIndexRequestedForLoading;
    }
    public bool IsSceneRequestedForLoadingNow(SceneIndexEnum sceneIndex)
    {
        return _sceneIndexRequestedForLoading == sceneIndex;
    }

    public SceneController GetLastSceneControllerOnScreen()
    {
        if (_additiveSceneStack.Count > 0)
        {
            return _additiveSceneStack.Peek();
        }

        // no additive screens in game, return current
        return _currentScene;
    }

    IEnumerator RemoveSceneFromMemory(SceneController sceneController)
    {
        if (sceneController != null)
        {
            yield return StartCoroutine(sceneController.OnDispose());
            if (sceneController != null && sceneController.gameObject != null)
            {
                Destroy(sceneController.gameObject);
            }
        }
    }

    static bool IsSceneLoaded(string sceneName)
    {
        Scene loadedScene = SceneManager.GetSceneByName(sceneName);
        return loadedScene.isLoaded;
    }

    static bool IsLastScene(string sceneName)
    {
        return SceneManager.GetSceneAt(0).name == sceneName;
    }

    SceneController GetSceneController(GameObject[] rootObjects)
    {
        SceneController toReturn = null;

        if (rootObjects == null || rootObjects.Length <= 0) return toReturn;

        foreach (var item in rootObjects)
        {
            if (item == null) continue;

            toReturn = item.GetComponentInChildren<SceneController>();
            if (toReturn != null)
            {
                break;
            }
        }

        return toReturn;
    }

    #endregion
}
