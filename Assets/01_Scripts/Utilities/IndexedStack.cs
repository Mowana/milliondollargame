﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndexedStack<T> : IEnumerable<T>
{
    List<T> _list = new List<T>();
    HashSet<T> _hashSet = new HashSet<T>();

    public void Push(T itemToAdd)
    {
        _list.Add(itemToAdd);
        _hashSet.Add(itemToAdd);
    }

    public T Pop()
    {
        if (_list.Count > 0)
        {
            T toReturn = _list[_list.Count - 1];
            _list.Remove(toReturn);
            _hashSet.Remove(toReturn);
            return toReturn;
        }
        Debug.LogError("Collection has 0 elements. Unable to retrieve and remove the last one");
        return default(T);
    }

    public T Peek()
    {
        if (_list.Count > 0)
        {
            return _list[_list.Count - 1];
        }
        Debug.LogError("Collection has 0 elements. Unable to retrieve last one");
        return default(T);
    }

    public bool Remove(T itemToRemove)
    {
        _hashSet.Remove(itemToRemove);
        bool toReturn = _list.Remove(itemToRemove);
        return toReturn;
    }

    public int Count
    {
        get
        {
            return _list.Count;
        }
    }

    public IEnumerator<T> GetEnumerator()
    {
        return _list.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return _list.GetEnumerator();
    }

    internal T Find(Predicate<T> p)
    {
        return _list.Find(p);
    }

    /// <summary>
    /// Custom indexer
    /// </summary>
    /// <param name="i">index to lookup</param>
    /// <returns></returns>
    public T this[int i]
    {
        get
        {
            return _list[i];
        }
        set
        {
            _list[i] = value;
        }
    }
}
