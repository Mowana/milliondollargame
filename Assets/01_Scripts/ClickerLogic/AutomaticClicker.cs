﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for clickeable elements 
/// </summary>
public class AutomaticClicker : MonoBehaviour {

    private float _generatedResource = 0;
    public float GeneratedResource
    {
        get { return _generatedResource; }
    }

    [SerializeField]
    private float _baseResourcesPerSecond = 1f;

    [SerializeField]
    private float _baseResourceGeneratedPerClick = 1f;

    [SerializeField]
    private float _coldownBetweenClicks = 0.25f;

    private bool _inClickColdown = false;

    /// <summary>
    /// Use this method for initialization
    /// </summary>
    public void Initialize()
    {
        StartCoroutine(AutomaticClick());
    }

    protected IEnumerator AutomaticClick()
    {
        while(true)
        {
            // Update resource
            _generatedResource += Time.deltaTime * _baseResourcesPerSecond;
        }
    }

    /// <summary>
    /// Use this method to request the increase of resources manually (by player input)
    /// </summary>
    public void ExecuteManualClick()
    {
        if (_inClickColdown)
            return;

        _generatedResource += _baseResourceGeneratedPerClick;

        StartCoroutine(ClickColdown());
    }

    protected IEnumerator ClickColdown()
    {
        _inClickColdown = true;
        yield return new WaitForSeconds(_coldownBetweenClicks);
        _inClickColdown = false;
    }
}
